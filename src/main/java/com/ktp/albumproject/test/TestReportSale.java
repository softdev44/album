/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.albumproject.test;

import com.ktp.albumproject.model.ReportSale;
import com.ktp.albumproject.service.ReportService;
import java.util.List;

/**
 *
 * @author acer
 */
public class TestReportSale {

    public static void main(String[] args) {
        ReportService reportService = new ReportService();
        List<ReportSale> report = reportService.getreportSaleByDay();
        for (ReportSale reportSale : report) {
            System.out.println(reportSale);
        }

        List<ReportSale> reportMonth = reportService.getreportSaleByMonth();
        for (ReportSale reportSale : reportMonth) {
            System.out.println(reportSale);
        }

        List<ReportSale> reportMonthByYear = reportService.getreportSaleByYear("2013");
        for (ReportSale reportSale : reportMonthByYear) {
            System.out.println(reportSale);
        }
    }
}
